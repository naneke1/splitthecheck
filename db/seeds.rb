# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Restaurant.delete_all
Restaurant.create(name: 'Chick-fil-A', location: 'Carrollton', upVotes: 2, downVotes: 1)
Restaurant.create(name: 'Moes', location: 'Carrollton', upVotes: 3, downVotes: 1)
Restaurant.create(name: 'Publix Deli', location: 'Carrollton', upVotes: 1, downVotes: 2)
Restaurant.create(name: 'Burger King', location: 'Carrollton', upVotes: 1, downVotes: 1)
Restaurant.create(name: 'McDonalds', location: 'Carrollton', upVotes: 0, downVotes: 3)
Restaurant.create(name: 'Border', location: 'Carrollton', upVotes: 2, downVotes: 0)
Restaurant.create(name: 'Rockys', location: 'Carrollton', upVotes: 0, downVotes: 0)
Restaurant.create(name: 'Olive Garden', location: 'Carrollton', upVotes: 5, downVotes: 2)
Restaurant.create(name: 'Longhorn', location: 'Carrollton', upVotes: 5, downVotes: 1)
Restaurant.create(name: 'CookOut', location: 'Carrollton', upVotes: 3, downVotes: 1)
