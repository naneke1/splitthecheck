class Restaurant < ApplicationRecord
  validates :name, :location, :upVotes, :downVotes, presence: true
def self.search(search)
  where("Name LIKE ? OR Location LIKE ?", "%#{search}%", "%#{search}%") 
end
end
