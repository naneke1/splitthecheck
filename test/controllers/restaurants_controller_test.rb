require 'test_helper'

class RestaurantsControllerTest < ActionDispatch::IntegrationTest
  fixtures :restaurants
  setup do
    @restaurant = restaurants(:one)
  end

  test "should get index" do
    get restaurants_url
    assert_response :success
  end

  test "sould get index if search by name" do
    get restaurants_url, params: {search: "CookOut"}
    assert_equal(1, assigns[:restaurants].count)
    assert_equal("CookOut", assigns[:restaurants][0].name )
  end

  test "sould not get index if name not there" do
    get restaurants_url, params: {search: "Joes"}
    assert_equal(0, assigns[:restaurants].count)
    assert_empty(assigns[:restaurants])
  end

  test "sould get index if search by location" do
    get restaurants_url, params: {search: "Douglasville"}
    assert_equal(1, assigns[:restaurants].count)
    assert_equal("Douglasville", assigns[:restaurants][0].location)
  end

  test "sould not get index if location not there" do
 get restaurants_url, params: {search: "Orlando"}
    assert_equal(0, assigns[:restaurants].count)
    assert_empty(assigns[:restaurants])
  end

  test "sould get index if search by location and name" do
    get restaurants_url, params: {search: "Atlanta"}
    assert_equal(2, assigns[:restaurants].count)
    assert_equal("Martins", assigns[:restaurants][0].name)
    assert_equal("Atlanta", assigns[:restaurants][0].location)
    assert_equal("McDonalds", assigns[:restaurants][1].name)
    assert_equal("Atlanta", assigns[:restaurants][1].location)
    get restaurants_url, params: {search: "Martins"}
    assert_equal("Martins", assigns[:restaurants][0].name)
    
  end

   test "sould not get index if location and name not there" do
    get restaurants_url, params: {search: "Orlando"}
    assert_equal(0, assigns[:restaurants].count)
    assert_empty(assigns[:restaurants])
    get restaurants_url, params: {search: "Wendys"}
    assert_equal(0, assigns[:restaurants].count)
    assert_empty(assigns[:restaurants])
  end


  test "should get new" do
    get new_restaurant_url
    assert_response :success
  end

  test "should create restaurant" do
    assert_difference('Restaurant.count') do
      post restaurants_url, params: { restaurant: { downVotes: @restaurant.downVotes, location: @restaurant.location, name: @restaurant.name, upVotes: @restaurant.upVotes } }
    end

    assert_redirected_to restaurant_url(Restaurant.last)
  end

  test "should show restaurant" do
    get restaurant_url(@restaurant)
    assert_response :success
  end

  test "should increment up votes" do
    get upvote_restaurant_url(@restaurant)
    @restaurant.reload
    assert_equal 5, @restaurant.upVotes
  end

  test "should increment down votes" do
    get downvote_restaurant_url(@restaurant)
    @restaurant.reload
    assert_equal 3, @restaurant.downVotes
  end
end
